const MongoClient = require('mongodb').MongoClient;
const URL = 'mongodb://localhost:4545/';

MongoClient.connect(URL, { useNewUrlParser: true }, function (err, db) {
  if (err) throw err;

  let dbo = db.db('vt-products');

  var data = [
    { barcode: "05050083545191", brandName: "Kellogg's", name: "Rice Krispies", image: "" },
    { barcode: "00078000152401", brandName: "Canada Dry", name: "Ginger Ale", image: "" },
    { barcode: "00068274911620", brandName: "Nestle", name: "Pure Life Splash", image: "" },
    { barcode: "00811571013579", brandName: "Google", name: "Chromecast", image: "https://d2b9vdin3yve6y.cloudfront.net/10a19090-ca14-4295-8e21-b19359a529a4.jpg, https://d2b9vdin3yve6y.cloudfront.net/24334296-900d-4564-ad56-109783e2b15b.jpg, https://d2b9vdin3yve6y.cloudfront.net/07806299-ac58-4510-88d1-354f9672bea7.jpg" }];

    data = data.map(x => {
      x.barcode = x.id || x.barcode;
      x.addedBy = x.addedBy ? x.addedBy : 'Datakick Dataset';
      x.addedAt = x.addedAt ? x.addedAt : new Date().toString();
      x.updatedAt = x.updatedAt ? x.updatedAt : new Date().toString();
      delete x.id;
      return x;
    });
  
    dbo.collection('personal-products').insertMany(data, function (err, res) {
      if (err) throw err;
      console.log(res);
      console.log('Number of documents inserted: ' + res.insertedCount);
      db.close();
    });
  });