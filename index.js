const express = require('express');
const bodyParser = require('body-parser');
const crypto = require('crypto-browserify');
const DB = require('./services/db').productDB;
const L = require('./services/logger');
const personalRoutes = require('./subroutes/personal-products');
require('dotenv').config();

// Control Variables
let PORT = process.env.PORT;

const app = express();
app.use( bodyParser.json() );
app.use(bodyParser.urlencoded({ extended: false }));
app.use(`/vetinventory/personal/`, personalRoutes);

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	L.logHit(req);
  next();
});

function authGuard(req, res, next) {
	const d = new Date();
  d.setTime( d.getTime() + d.getTimezoneOffset() * 60 * 1000 );
	let dateString = d.toDateString();
	const key = crypto.createCipher('aes-256-ecb', process.env.SALT);
	let pass = key.update(dateString, 'utf8', 'hex');
	pass += key.final('hex');
  if (pass != req.params.key) {
    res.status(403).send('You do not have permission to do this');
	}
	
	next();
}

const API = '/vetinventory/:key';
const routes = {
	// For Vet Products
  insertMany: `${API}/product/insert-many`,
  insertOne: `${API}/product/insert-one`,
  getItem: `${API}/product/does-exist`,
  getAllItems: `${API}/product/get-all`,
  updateItem: `${API}/product/update-item`,
  deleteItem: `${API}/product/delete-item`,
  getRecent: `${API}/product/get-recent`,
  getCount: `${API}/product/get-count`,
	getdbStats: `${API}/product/collection/statistics`,
	getBrandCounts: `${API}/product/collection/brand-counts`,
	getItemsBetweenDates: `${API}/product/collection/items-between/:start/:end/only`
};

/************************************

	VETINVENTORY ROUTES

************************************/

/*********** Get Logs */

app.get(`${API}/diagnostics`, function(req, res) {
	res.status(200).send(L.stats);
});

app.get('/',function(req, res) {
	res.status(200).send('Hello!');
});

app.get(routes.getAllItems, authGuard, function (req, res) {
	let operation = DB.getAllEntries();
	operation.then(result => {
		res.status(200).send(result);
	})
	.catch(err => {
		res.status(200).send(err);
	})
})

app.post(routes.insertMany, authGuard , function(req,res) {
	let pck = req.body;
	let operation = DB.insertMany(pck);
	operation.then(result => {
		if (result.success && result.data) {
			res.status(200).send(result.data.ops[0]);
		} else if (result.success) {
			result.data.then(foundData => {
				res.status(200).send(foundData);
			})
		} else {
			res.status(200).send({status: "Added new items"});
		}
	})
});

// When a new found item is being inserted
app.post(routes.insertOne, authGuard , function(req,res) {
	let pck = req.body;
	let operation = DB.insertOne(pck);
	operation.then(result => {
		if (result.success && result.data) {
			res.status(200).send(result.data.ops[0]);
		} else if (result.success) {
			result.data.then(foundData => {
				res.status(200).send(foundData);
			})
		} else {
			res.status(200).send({status: "Added New Item"});
		}
	})
});

// Check if an item exists in our db already 
app.post(routes.getItem, authGuard , function (req, res) {
	let pck = req.body;
	let operation = DB.doesExist(pck.id);
	operation.then(result => {
		if (result) {
			res.status(200).send(result);
		} else {
			res.status(200).send({status: "Not found"});
		}
	})
});

// Update an item in db
app.post(routes.updateItem, authGuard , function (req, res) {
	let pck = req.body;
	let operation = DB.updateItem(pck.barcode, pck);
	operation.then(result => {
		if (result) {
			res.status(200).send({status: 'success'});
		} else {
			res.status(200).send({status: 'fail'});
		}
	})
	.catch (err => {console.log(err)});
})

app.post(routes.deleteItem, authGuard , function (req,res) {
	let pck = req.body;
	let operation = DB.deleteItem(pck.id);
	operation.then(result => {
		if (result) {
			res.status(200).send(result);
		} else {
			res.status(200).send({status: 'Update Successful'});
		}
	})
})

app.get(routes.getRecent, authGuard , function(req,res) {
	let operation = DB.getRecentEntries();
	operation.then(result => {
		if (result) {
			res.status(200).send(result);
		} else {
			res.status(200).send({status: 'Something went wrong when querying'});
		}
	})
})

app.get(routes.getCount, authGuard , function (req, res) {
	let operation = DB.getItemCount();
	operation.then(result => {
		if (result) {
			res.status(200).send({count: result});
		} else {
			res.status(200).send({status: 'Could not get count'})
		}
	})
})

app.get(routes.getdbStats, authGuard , function (req, res) {
	let operation = DB.getStatistics();
	operation.then(result => {
		if (result) {
			res.status(200).send(result);
		} else {
			res.status(200).send({status: 'Something went wrong'});
		}
	});
})

app.post(routes.getBrandCounts, authGuard, function(req, res) {
	if (req.body.brands) {
		let operation = DB.getManyItemCount(req.body.brands);
		operation.then(result => {
			if (result) {
				res.status(200).send(result);
			} else {
				res.status(200).send({status: 'Something went wrong'});
			}
		})
	}
})

app.get(routes.getItemsBetweenDates, authGuard, function(req,res) {
	start = req.params.start ? parseInt(req.params.start) || '' : '';
	end = req.params.end ? parseInt(req.params.end) || '' : '';

	if (start && end) {
		let operation = DB.getItemsBetween(start, end);
		operation.then(result => {
			if (result) {
				res.status(200).send(result);
			} else {
				res.status(200).send({status: 'Something went wrong'});
			}
		})
	} else {
		res.status(403).send({status: 'Invalid Request'});
	}
})

// -----------------------------------------------------------------------------


// Start Server
app.listen(PORT,function(){
	console.log(`Sever running on ${PORT}.\nPress "Ctrl + Z" followed by "bg" to push this to the background`);
});


// Helper functions -----------------------------


// Show meaningful error message
process.on('unhandledRejection', error => {
  // Will print "unhandledRejection err is not defined"
  if (process.env.NODE_ENV !== 'prod') {
		console.log('unhandledRejection', error.message);
	}
});
