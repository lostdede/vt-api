

class Logger {
  
  constructor(){
    this.hits = [];
    this.errors = [];
    this.dbUpdates = [];
  };

  get stats() {
    return {
      hits: this.hits,
      errors: this.errors,
      events: this.dbUpdates
    };
  }

  // Store when a Route is Hit
  logHit(req) {
    this.hits.push({
      route: req.originalUrl,
      from: req.connection.remoteAddress,
      time: new Date().toString()
    });
  }

  // Store when an error occurs
  logError(err) {
    this.errors.push({
      error: err,
      time: new Date().toString()
    });
  }
  
  logDBEvent(event) {
    this.dbUpdates.push(event);
  }
}

module.exports = new Logger();