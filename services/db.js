const ProductFinder = require('./product-finder');
const Log = require('./logger');
const moment = require('moment');
const MongoClient = require('mongodb').MongoClient;
const URL = process.env.MONGO_URL || 'mongodb://localhost:4545/';

class DataBase {

  constructor(collection) {
    MongoClient.connect(URL, { useNewUrlParser: true })
      .then((db) => {
        this.DB = db.db('vt-products');

        this.Collection = this.DB.collection(collection);
      })
      .catch((err) => {
        throw err;
      })
  }

  async getAllEntries() {
    let search = await this.Collection.find({}).toArray()
    .then(result => {
      return result;
    })

    return search;
  }

  async getItemCount() {
    let search = await this.Collection.countDocuments({})
    .then(result => {
      return result
    })

    return search;
  }

  async getIncompleteItemsCount() {
    let search = await this.Collection.countDocuments({name: ""})
    .then(result => {
      return result;
    })

    return search;
  }

  async getRecentEntries(no = 5) {
    let search = await this.Collection.find().sort({_id:-1}).limit(no).toArray()
    .then(results => {
      return results;
    })

    return search;
  }

  // Check if an Item exists in our DB
  async doesExist(barcode) {
    const query = {barcode: barcode};

    async function handleVerdict (result) {
      if (result && result.length != 0) {
        return result[0];
      } else {
        return {};
      }
    }

    let verdict = await this.Collection.find(query).toArray()
    .then(result => handleVerdict(result));

    return verdict;
  }

  async insertMany(arr) {
    let data = arr.map(itm => {
        itm.barcode = itm.id;
        itm.name = itm.name ? itm.name : "";
        itm.brandName = itm.brandName ? itm.brandName : "";
        itm.images = itm.images ? itm.images : "";
        itm.msrp = itm.msrp ? itm.msrp : 'Not provided';
        itm.addedBy = itm.addedBy ? itm.addedBy : 'Not recorded';
        itm.addedAt = itm.addedAt ? itm.addedAt : new Date();
        itm.updatedAt = itm.updatedAt ? itm.updatedAt : new Date();
        delete itm.id;
        delete itm.quantity;
        return itm;
    });

    let insert = await this.Collection.insertMany(data)
    .then(result => {
      if (result) {
        return {success: true, data: result};
      } else {
        return {success: false, data: null};
      }
    });

    return insert;
  }

  async insertOne(itm) {
    itm.barcode = itm.id || itm.barcode;
    itm.name = itm.name ? itm.name : "";
    itm.brandName = itm.brandName ? itm.brandName : "";
    itm.images = itm.images ? itm.images : "";
    itm.msrp = itm.msrp ? itm.msrp : 'Not provided';
    itm.addedBy = itm.addedBy ? itm.addedBy : 'Not recorded';
    itm.addedAt = itm.addedAt ? itm.addedAt : new Date();
    itm.updatedAt = itm.updatedAt ? itm.updatedAt : new Date();
    delete itm.id;
    delete itm.quantity;

    const query = { barcode: itm.barcode };

    let queryResult = await this.Collection.find(query).toArray()
    .then(result => {
      if (result && result.length != 0) {
        return true;
      } else {
        return false;
      }
    });

    if (!queryResult) {
      let res = this.Collection.insertOne(itm).then(result => {
        if (result) {
          return {sucess: true, data: result};
        } else {
          return {success: false, data: null};
        }
      })
      return res;
    } else {
      return {success: true, data: this.doesExist(itm.barcode)};
    }
  }


  async updateItem(barcode, data) {
    delete data._id;
    const rt = await this.Collection.updateOne({barcode: barcode}, {'$set': data})
    .then(result => {
      Log.logDBEvent(`Item updated: ${barcode}`);
      return result;
    })
    .catch(err => {
      console.log(err);
      Log.logDBEvent(`Item update failed: ${barcode}\n Details: ${err}`);
    });

    return rt;
  }

  async deleteItem(barcode) {
    this.Collection.deleteOne({barcode: barcode})
    .then(result => {
      Log.logDBEvent(`Item Deleted: ${barcode}`);
      return result;
    })
    .catch(err => {
      Log.logDBEvent(`Item delete failed: ${barcode}\n Details: ${err}`);
    })
  }

  /* Take a brands and return how many items it has
   * @params brands -> String that is a brands
   */
  async getItemCount(brand) {
    const items = await this.Collection.countDocuments({brandName: brand})
      .then(results => { return results });
    return {
      brandName: brand,
      itemCount: items
    };
  }

  /* Take a list of brands and return how many items each brand has
   * @params brands -> Array of strings that are brands
   */
  async getManyItemCount(brands) {
    let result = [];
    for(let i = 0; i < brands.length; i++) {
      const brandItem = await this.getItemCount(brands[i])
      .then(re => {return re});
      result.push(brandItem);
    }
    return result;
  }

  async getStatistics() {
    const count = await this.Collection.countDocuments({})
    .then(result => { return result })
    .catch(err => console.log(err));

    const noNames = await this.Collection.find({$or: [ { name: '' }, { name: 'Unknown' }]}).toArray()
    .then(result => { return result.length; })
    .catch(err => console.log(err));

    const noBrands = await this.Collection.find({$or: [ { brandName: '' }, { brandName: 'Unknown' }]}).toArray()
    .then(result => { return result.length; })
    .catch(err => console.log(err));


    const brands = await this.Collection.distinct("brandName", {brandName: { $not: /""/}})
    .then(results => { 
      return results 
    });

    return {
      productCount: count,
      brandList: brands,
      noNameItems: noNames,
      noBrandItems: noBrands 
    };
  }

  async getItemsBetween(start, end) {
    start = new Date(start).toISOString();
    end = new Date(end).toISOString();
    const items = await this.Collection.find({"updatedAt": {"$gte": end }}).toArray()
    .then(result => {return result})
    .catch(err => console.log(err));

    return items;
  }
}

module.exports.productDB = new DataBase('products');
module.exports.personalDB = new DataBase('personal-products');