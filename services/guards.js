const crypto = require('crypto-browserify');

function authGuard(req, res, next) {
	const d = new Date();
  d.setTime( d.getTime() + d.getTimezoneOffset() * 60 * 1000 );
	let dateString = d.toDateString();
	const key = crypto.createCipher('aes-256-ecb', process.env.SALT);
	let pass = key.update(dateString, 'utf8', 'hex');
	pass += key.final('hex');
  if (pass != req.params.key) {
		console.log(req.params.key);
    res.status(403).send('You do not have permission to do this');
	}
	
	next();
}

module.exports.authGuard = authGuard;