const express = require('express');
const router = express.Router();
const authGuard = require('./../services/guards').authGuard;
const PDB = require('./../services/db').personalDB;
const routePre = ':key';
const routes = {
	// For Personal Products
	insertMany: `/${routePre}/product/insert-many`,
  insertOne: `/${routePre}/product/insert-one`,
  getItem: `/${routePre}/product/does-exist`,
  getAllItems: `/${routePre}/product/get-all`,
  updateItem: `/${routePre}/product/update-item`,
  deleteItem: `/${routePre}/product/delete-item`,
  getRecent: `/${routePre}/product/get-recent`,
  getCount: `/${routePre}/product/get-count`,
	getdbStats: `/${routePre}/product/collection/statistics`,
	getBrandCounts: `/${routePre}/product/collection/brand-counts`,
	getItemsBetweenDates: `/${routePre}/product/collection/items-between/:start/:end/only`,
};

// ---------------------------------- PERSONAL DB ROUTES

router.get(routes.getAllItems, authGuard, function (req, res) {
	let operation = PDB.getAllEntries();
	operation.then(result => {
		res.status(200).send(result);
	})
	.catch(err => {
		res.status(200).send(err);
	})
})

router.post(routes.insertMany, authGuard , function(req,res) {
	let pck = req.body;
	let operation = PDB.insertMany(pck);
	operation.then(result => {
		if (result.success && result.data) {
			res.status(200).send(result.data.ops[0]);
		} else if (result.success) {
			result.data.then(foundData => {
				res.status(200).send(foundData);
			})
		} else {
			res.status(200).send({status: "Added new items"});
		}
	})
});

// When a new found item is being inserted
router.post(routes.insertOne, authGuard , function(req,res) {
	let pck = req.body;
	let operation = PDB.insertOne(pck);
	operation.then(result => {
		if (result.success && result.data) {
			res.status(200).send(result.data.ops[0]);
		} else if (result.success) {
			result.data.then(foundData => {
				res.status(200).send(foundData);
			})
		} else {
			res.status(200).send({status: "Added New Item"});
		}
	})
});

// Check if an item exists in our db already 
router.post(routes.getItem, authGuard , function (req, res) {
	let pck = req.body;
	let operation = PDB.doesExist(pck.id);
	operation.then(result => {
		if (result) {
			res.status(200).send(result);
		} else {
			res.status(200).send({status: "Not found"});
		}
	})
});

// Update an item in db
router.post(routes.updateItem, authGuard , function (req, res) {
	let pck = req.body;
	let operation = PDB.updateItem(pck.barcode, pck);
	operation.then(result => {
		if (result) {
			res.status(200).send({status: 'success'});
		} else {
			res.status(200).send({status: 'fail'});
		}
	})
	.catch (err => {console.log(err)});
})

router.post(routes.deleteItem, authGuard , function (req,res) {
	let pck = req.body;
	let operation = PDB.deleteItem(pck.id);
	operation.then(result => {
		if (result) {
			res.status(200).send(result);
		} else {
			res.status(200).send({status: 'Update Successful'});
		}
	})
})

router.get(routes.getRecent, authGuard , function(req,res) {
	let operation = PDB.getRecentEntries();
	operation.then(result => {
		if (result) {
			res.status(200).send(result);
		} else {
			res.status(200).send({status: 'Something went wrong when querying'});
		}
	})
})

router.get(routes.getCount, authGuard , function (req, res) {
	let operation = PDB.getItemCount();
	operation.then(result => {
		if (result) {
			res.status(200).send({count: result});
		} else {
			res.status(200).send({status: 'Could not get count'})
		}
	})
})

router.get(routes.getdbStats, authGuard , function (req, res) {
	let operation = PDB.getStatistics();
	operation.then(result => {
		if (result) {
			res.status(200).send(result);
		} else {
			res.status(200).send({status: 'Something went wrong'});
		}
	});
})

router.post(routes.getBrandCounts, authGuard, function(req, res) {
	if (req.body.brands) {
		let operation = PDB.getManyItemCount(req.body.brands);
		operation.then(result => {
			if (result) {
				res.status(200).send(result);
			} else {
				res.status(200).send({status: 'Something went wrong'});
			}
		})
	}
})

router.get(routes.getItemsBetweenDates, authGuard, function(req,res) {
	start = req.params.start ? parseInt(req.params.start) || '' : '';
	end = req.params.end ? parseInt(req.params.end) || '' : '';

	if (start && end) {
		let operation = PDB.getItemsBetween(start, end);
		operation.then(result => {
			if (result) {
				res.status(200).send(result);
			} else {
				res.status(200).send({status: 'Something went wrong'});
			}
		})
	} else {
		res.status(403).send({status: 'Invalid Request'});
	}
})

module.exports = router;